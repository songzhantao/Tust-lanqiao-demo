package org.lanqiao.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

/**
 * 中文乱码处理过滤器
 * @author songzt@lanqiao.org
 *
 */
@WebFilter("/*")
public class CharsetFiler  implements Filter{

	@Override
	public void destroy() {
		
	}

	/**
	 * 设置请求和响应的数据编码格式
	 * Get方式的编码可以在tomcat中设置URIEnconding="utf-8";
	 */
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		  request.setCharacterEncoding("utf-8");
		  response.setCharacterEncoding("utf-8");
		  chain.doFilter(request, response);
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
	
		
	}

}
