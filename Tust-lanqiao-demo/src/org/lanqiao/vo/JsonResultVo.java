package org.lanqiao.vo;

import java.io.Serializable;
/**
 * 封装Json格式代替HashMap
 * @author songzt@lanqiao.org
 * @param <T>
 *
 */
public class JsonResultVo<T>  implements Serializable{
		private  String code;
		private  String info;
		private  T   data;
		
		public  JsonResultVo(){
			
		}
		
		public  JsonResultVo(String code, String info,T   data){
			this.code=code;
			this.info=info;
			this.data=data;
		}
		
		public  JsonResultVo(String code, String info){
			this.code=code;
			this.info=info;
			
		}
		
		public String getCode() {
		return code;
		}
		public T getData() {
			return data;
		}
		public String getInfo() {
			return info;
		}
		public void setCode(String code) {
			this.code = code;
		}
		public void setData(T data) {
			this.data = data;
		}
		public void setInfo(String info) {
			this.info = info;
		}
	
	
	
}
