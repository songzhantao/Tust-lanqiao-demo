package org.lanqiao.util;

public class StringUtils {

	/**
	 * 如果是NUll 返回 空串
	 * @param object
	 * @return
	 */
	public static  String  toSpaceSting(Object object){
		
		if(object==null){
			return  "";
		}else{
			return   object.toString();
		}
	}
	
	/**
	 * 如果是NUll 返回null
	 * @param object
	 * @return
	 */
	public static  String  toNullSting(Object object){
		
		if(object==null){
			return  "";
		}else{
			return   object.toString();
		}
	}
}
