package org.lanqiao.util;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
/**
 * 序列化Json
 * @author 
 *
 */
public class BeanToJsonWriter {

	/**
	 * 把result转换成Json字符串
	 * @param resposne
	 * @param result
	 */
	public static void write(HttpServletResponse resposne ,Object  jsonResult){
		resposne.setCharacterEncoding("utf-8");
		resposne.setContentType("application/json");
		Gson gson =new Gson();
		String json=gson.toJson(jsonResult);
		PrintWriter writer=null;
		try {
			writer = resposne.getWriter();
		} catch (IOException e) {
			e.printStackTrace();
		}
		writer.print(json);
		writer.close();
	}
}
