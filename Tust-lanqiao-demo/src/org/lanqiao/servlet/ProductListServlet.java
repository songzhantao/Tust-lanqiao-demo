package org.lanqiao.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.lanqiao.util.SqlUtil;

import com.google.gson.Gson;

@WebServlet("/product/list")
public class ProductListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse resposne) throws ServletException, IOException {
		//1、获取参数  
		//2、编写一个查询的Sql语句
		String sql="select  * from product";
		List list =SqlUtil.executeQuery(sql);
		resposne.setContentType("application/json");
		Gson gson =new Gson();
		String json=gson.toJson(list);
		PrintWriter  writer  =resposne.getWriter();
		writer.print(json);
		writer.close();
		
		
		
	}
}
