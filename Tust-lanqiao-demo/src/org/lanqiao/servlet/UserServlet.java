package org.lanqiao.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.lanqiao.util.SqlUtil;

import com.google.gson.Gson;

/**
 * 根据ID查询用户信息 返回一条数据
 * @author songzt@lanqiao.org
 *
 */
@WebServlet("/user/byid")
public class UserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse resposne) throws ServletException, IOException {
		//设置中文
		//获取参数
		request.setCharacterEncoding("utf-8");
		String id =request.getParameter("params_id");
		String sql="select * from  user where id =?";
		List<HashMap<String ,Object>>   list  =SqlUtil.executeQuery(sql, id);
		HashMap user=null;
		if(list!=null && list.size()>0){
			user =list.get(0);
		}
		
		
		resposne.setCharacterEncoding("utf-8");
		resposne.setContentType("application/json");
		Gson gson =new Gson();
		String json=gson.toJson(user);
		PrintWriter  writer  =resposne.getWriter();
		writer.print(json);
		writer.close();
		
		
	}

}
