package org.lanqiao.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.lanqiao.util.BeanToJsonWriter;
import org.lanqiao.util.SqlUtil;
import org.lanqiao.vo.JsonResultVo;

import com.google.gson.Gson;
/**
 * 查询购物车下单商品
 * @author Administrator
 *
 */
@WebServlet("/car/query")
public class QueryCheckProsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse resposne) throws ServletException, IOException {
		//获取 要购买的购物车商品
		String [] ids =request.getParameterValues("ids");
		StringBuilder  sql =new StringBuilder(" select ");
		sql.append("  car.id ,");
		sql.append("  username , ");
		sql.append("  proid,");
		sql.append("  number ,");
		sql.append("  name ,");
		sql.append("  price ,");
		sql.append("  type ,");
		sql.append("  image ");
		sql.append("  from car ");
		sql.append("   left join product  ");
		sql.append("  on   car.proid=product.id  ");
		sql.append(" where  car.id in (   ");
		int l =ids.length;
		for(int i =0;i<l;i++){
			if(i==l-1){
				sql.append("  ?  ");
			}else{
				sql.append("  ? ,");
			}
		
		}
		sql.append("  )   ");
		//应该处理异常，最初没有设计架构
		List<HashMap<String,Object >>  list =SqlUtil.executeQuery(sql.toString(), ids);
		BeanToJsonWriter.write(resposne, list);
		
	}
}
