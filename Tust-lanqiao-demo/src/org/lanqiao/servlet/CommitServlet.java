package org.lanqiao.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.lanqiao.util.SqlUtil;

import com.google.gson.Gson;

@WebServlet("/user/commit")
public class CommitServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse resposne) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		String id =request.getParameter("id");
		String username =request.getParameter("username");
		String email =request.getParameter("email");
		String password =request.getParameter("password");
		String telphone =request.getParameter("telphone");
		String nickname =request.getParameter("nickname");
		String sql ="update user set username=? , email=? , password=?,telphone=?,nickname=? where  id=?";
		HashMap map =new HashMap();
		
		try{
			SqlUtil.executeUpdate(sql, username,email,password,telphone,nickname,id);
			map.put("info", "修改成功");
		}catch(Exception ex){
			map.put("info", "修改失败 ");
			ex.printStackTrace();
		}
		
		resposne.setCharacterEncoding("utf-8");
		resposne.setContentType("application/json");
		Gson gson =new Gson();
		String json=gson.toJson(map);
		PrintWriter  writer  =resposne.getWriter();
		writer.print(json);
		writer.close();
		
		
		
		
		
		
	}
}
