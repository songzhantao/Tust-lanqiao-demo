package org.lanqiao.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.lanqiao.util.SqlUtil;

import com.google.gson.Gson;

/**
 * 查询购物车
 * @author 
 *
 */
@WebServlet("/car/list")
public class CarListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse resposne) throws ServletException, IOException {
		//1、获取登录人用户名或者临时用户名
		HttpSession session  =request.getSession(true);
		String username=String.valueOf(session.getAttribute("loginUserKey"));
		String buyer=String.valueOf(session.getAttribute("buyerKey"));
		
		//2、编写一个查询的Sql语句 按照购物临时用户和用户名（如果购买的先用了临时用户,后登陆了，以及结算必须登录）
		String sql="select car.id,username,proid,number ,name,price,type,image from car ";
		StringBuilder  sqlBuffer =new StringBuilder(sql);
		sqlBuffer.append("   left join product on   car.proid=product.id where  username in (?,?)   ");
		sqlBuffer.append(" and  orderid  is null  ");
		List list =SqlUtil.executeQuery(sqlBuffer.toString(),username,buyer);
		resposne.setCharacterEncoding("utf-8");
		resposne.setContentType("application/json"); 
		Gson gson =new Gson();
		String json=gson.toJson(list);
		PrintWriter  writer  =resposne.getWriter();
		writer.print(json);
		writer.close();
		
		
		
	}
}
