package org.lanqiao.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.lanqiao.util.SqlUtil;

import com.google.gson.Gson;

/**
 * 完成用户信息的查询
 * @author songzt@lanqiao.org
 *
 */
@WebServlet("/user/list")
public class ListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse resposne) throws ServletException, IOException {
		//1、获取参数  
		//2、编写一个查询的Sql语句
		String sql="select id,username,password,email,telphone,nickname from user";
		List list =SqlUtil.executeQuery(sql);
		resposne.setCharacterEncoding("utf-8");
		resposne.setContentType("application/json");
		Gson gson =new Gson();
		String json=gson.toJson(list);
		PrintWriter  writer  =resposne.getWriter();
		writer.print(json);
		writer.close();
		
		
		
	}
}
