package org.lanqiao.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.lanqiao.util.SqlUtil;

import com.google.gson.Gson;


/**
 * 利用数据库实现购物车
 * @author k301
 *
 */
@WebServlet("/product/join")
public class JoinCarServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		//获取加入购物车的商品的ID和数量
		String id =request.getParameter("id");
		//从Session中,获取登录用户 如果没有登录临时产生个临时用户
		String buyer="";
		HttpSession session =request.getSession(true);
		Object loginUserName =session.getAttribute("loginUserKey");
		//如果已经登录 购买者就是当前用户
		if(loginUserName!=null){
			buyer =String.valueOf(loginUserName);
		}else{
			
		  //如果没有登录 购买者就是临时用户	看看这个临时用户本次会话是否已经购买商品过(Session中有buyer说明买过，继续用这个临时用户)
			if(session.getAttribute("buyerKey")!=null){
				buyer=String.valueOf(session.getAttribute("buyerKey"));
			}else{
				//Temp+时间  就是临时用户的标志
				buyer="Temp"+System.currentTimeMillis() ;
				session.setAttribute("buyerKey", buyer);
			}
			
		
			
			
		}
		//把购买者的信息，存入Session，这样进入购物车页面才能关联数据库的记录 ，否则你知道是谁买的 ？	
		
		//像购物车表插入记录,默认购买一件，暂时不考虑重复性，状态是0表示没有支付
		String sql ="insert into car(username,proid,number,status) values (?,?,1,0)";
		HashMap map =new HashMap();
		try{
			SqlUtil.executeUpdate(sql, buyer,id);
			map.put("info", "已经加入购物车");
		}catch(Exception ex){
			map.put("info", "加入购物车异常 ");
			ex.printStackTrace();
		}
		
		response.setCharacterEncoding("utf-8");
		response.setContentType("application/json");
		Gson gson =new Gson();
		String json=gson.toJson(map);
		PrintWriter  writer  =response.getWriter();
		writer.print(json);
		writer.close();
		
	}
  
}
