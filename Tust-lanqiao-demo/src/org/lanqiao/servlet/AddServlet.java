package org.lanqiao.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.lanqiao.util.SqlUtil;

import com.google.gson.Gson;


@WebServlet("/user/add")
public class AddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		String username =request.getParameter("username");
		String email =request.getParameter("email");
		String password =request.getParameter("password");
		String telphone =request.getParameter("telphone");
		String nickname =request.getParameter("nickname");
		String sql ="insert into user(username,email,password,telphone,nickname)  values (?,?,?,?,?)";
		HashMap map =new HashMap();
		
		try{
			SqlUtil.executeUpdate(sql, username,email,password,telphone,nickname);
			map.put("info", "添加完毕");
		}catch(Exception ex){
			map.put("info", "添加异常 ");
			ex.printStackTrace();
		}
		
		response.setCharacterEncoding("utf-8");
		response.setContentType("application/json");
		Gson gson =new Gson();
		String json=gson.toJson(map);
		PrintWriter  writer  =response.getWriter();
		writer.print(json);
		writer.close();
		
	}
  
}
