/**
 * auth.js 是专门用来做身份认证和授权的js,在前端需要做身份认证和授权页面引入js即可，前提是必须配合lanqiao.js使用，而且是先引入lanqiao.js
 * 同时要和登录请求配合使用，登录请求成功后，要把后端生成的token  以loginTokenKey为key存入Session（sessionStorage）对象中
 * login.html是当前工程中的登录页面路径
 */

//判断用户是否登录
var     loginTokenValue = _Global_Session.Get("loginTokenKey");
if(loginTokenValue==null){
		location.href="login.html";
}



//判断权限是否有效