//本地存储
if(window.localStorage){
	var  _Global_Local={
			Set:function(key ,data){
				if (key != "") {
		            if (data) {
		                var lsobj = window.localStorage;
		                var datajson = JSON.stringify(data);
		                lsobj.setItem(key, datajson);
		            }
        		}
				
			},
			Get:function(key ,data ){
				if (key != "") {
		            var data = null;
		            var lsdata = window.localStorage;
		            try {
		                var datajson = lsdata.getItem(key);
		                datajson = JSON.parse(datajson);
		                data = datajson;
		            } catch (e) {
		
		            } finally {
		                return data;
		            }
       			}
			},
			//返回数组
			Append:function(key ,data){
				
				if(!window.localStorage.getItem(key)){
					var arr=new Array();
					arr.push(data);
					this.Set(key ,arr);
				}else{
					//不空
					var lsdata = window.localStorage;
					var datajson = lsdata.getItem(key);
		            datajson = JSON.parse(datajson);
		        	 if(datajson instanceof Array){
		            	datajson.push(data);
		            	this.Set(key,datajson);
		            }
		            
				}
			},
			
			Clear:function(){
				window.localStorage.clear();
			},
			Remove:function(key){
				window.localStorage.removeItem(key);
			}
		
	};
	
}else{
	alert("抱歉，您的浏览器不支持localStorage");
}


/**
 * sessionStorage 属性允许你访问一个 session Storage 对象。它与 localStorage 相似，不同之处在于 localStorage 里面存储的数据没有过期时间设置，
 * 而存储在 sessionStorage 里面的数据在页面会话结束时会被清除。页面会话在浏览器打开期间一直保持，并且重新加载或恢复页面仍会保持原来的页面会话。
 * 在新标签或窗口打开一个页面会初始化一个新的会话，这点和 session cookies 的运行方式不同。
 */
//本地存储
if(window.sessionStorage){
	var  _Global_Session={
			Set:function(key ,data){
				if (key != "") {
		            if (data) {
		                var lsobj = window.sessionStorage;
		                var datajson = JSON.stringify(data);
		                lsobj.setItem(key, datajson);
		            }
        		}
				
			},
			Get:function(key ,data ){
				if (key != "") {
		            var data = null;
		            var lsdata = window.sessionStorage;
		            try {
		                var datajson = lsdata.getItem(key);
		                datajson = JSON.parse(datajson);
		                data = datajson;
		            } catch (e) {
		
		            } finally {
		                return data;
		            }
       			}
			},
			//返回数组
			Append:function(key ,data){
				
				if(!window.sessionStorage.getItem(key)){
					var arr=new Array();
					arr.push(data);
					this.Set(key ,arr);
				}else{
					//不空
					var lsdata = window.sessionStorage;
					var datajson = lsdata.getItem(key);
		            datajson = JSON.parse(datajson);
		        	 if(datajson instanceof Array){
		            	datajson.push(data);
		            	this.Set(key,datajson);
		            }
		            
				}
			},
			
			Clear:function(){
				window.sessionStorage.clear();
			},
			Remove:function(key){
				window.sessionStorage.removeItem(key);
			}
		
	};
	
}else{
	alert("抱歉，您的浏览器不支持sessionStorage");
}





//从URL中截取？后面的参数
function  request(name) {
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
	var r = window.location.search.substr(1).match(reg);
	if (r != null)
		return decodeURI(r[2]);
	return null;
};
