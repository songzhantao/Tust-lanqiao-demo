/*
Navicat MySQL Data Transfer

Source Server         : root
Source Server Version : 50519
Source Host           : localhost:3306
Source Database       : test

Target Server Type    : MYSQL
Target Server Version : 50519
File Encoding         : 65001

Date: 2018-03-29 17:46:26
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for admin
-- ----------------------------
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `roleid` varchar(11) COLLATE utf8_bin DEFAULT NULL,
  `rolename` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of admin
-- ----------------------------
INSERT INTO `admin` VALUES ('1', 'root', '123456', 'admin', '管理员');
INSERT INTO `admin` VALUES ('2', 'system', '123456', 'admin', '管理员');

-- ----------------------------
-- Table structure for car
-- ----------------------------
DROP TABLE IF EXISTS `car`;
CREATE TABLE `car` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `proid` int(11) DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `orderid` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=88 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of car
-- ----------------------------
INSERT INTO `car` VALUES ('84', 'Temp1522316641392', '2', '1', '0', null);
INSERT INTO `car` VALUES ('85', 'Temp1522316641392', '1', '1', '0', null);
INSERT INTO `car` VALUES ('86', 'Temp1522316682463', '2', '1', '0', '1522316686247');
INSERT INTO `car` VALUES ('87', 'Temp1522316682463', '3', '1', '0', '1522316686247');

-- ----------------------------
-- Table structure for order
-- ----------------------------
DROP TABLE IF EXISTS `order`;
CREATE TABLE `order` (
  `id` varchar(255) COLLATE utf8_bin NOT NULL,
  `username` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `receiver` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `telphone` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `price` decimal(10,2) DEFAULT NULL,
  `ordertime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of order
-- ----------------------------
INSERT INTO `order` VALUES ('1522316645763', 'Temp1522316641392', 'sadf', 'afda', 'adfa', 'afdafdafda', '350.00', '2018-03-29 17:44:09');
INSERT INTO `order` VALUES ('1522316686247', 'Temp1522316682463', 'adf', 'adfa', 'adfa', 'adfafadfa', '183.00', '2018-03-29 17:44:54');

-- ----------------------------
-- Table structure for product
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `price` double(10,2) DEFAULT NULL,
  `type` char(255) COLLATE utf8_bin DEFAULT NULL,
  `capacity` int(255) DEFAULT NULL,
  `desc` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES ('1', '一双白鞋', '200.00', '阿迪达斯', '100', '阿迪非常昂贵，但是质量非常好', 'static/img/1.jpg');
INSERT INTO `product` VALUES ('2', '女孩小鞋', '150.00', '耐克', '100', '便宜', 'static/img/KKK3.png');
INSERT INTO `product` VALUES ('3', '3333', '33.00', '3333', '11', '11', 'static/img/KKK3.png');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `telphone` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `nickname` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'admin', '123456', '250354234@qq.com', '18810312041', '蓝桥涛哥');
INSERT INTO `user` VALUES ('2', 'Tust', '123456', 'Tust@qq.com', '18810312041', '科大一哥');
INSERT INTO `user` VALUES ('3', 'adfa', 'adfadadafa', 'adfa', 'adfa', 'afdad');
INSERT INTO `user` VALUES ('4', 'adfaadfadf', 'adfa', 'adfad', 'ada', 'adfa');
INSERT INTO `user` VALUES ('5', 'adsfa', 'adfa', 'adfa', 'adf', 'adfad');
INSERT INTO `user` VALUES ('6', 'adfadfad', 'adfa', 'adfadfa', 'adfada', 'adfad');
